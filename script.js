// 1 

let arr = ["travel", "hello", "eat", "ski", "lift"]
let arr2 = arr.filter(value => value.length > 3)
console.log(arr2.length)

// 2

let users = [
    {
        name: "Іван",
        age: 25,
        sex: "чоловіча",
    }, 
    {
        name: "Іра",
        age: 22,
        sex: "жіноча",
    }, 
    {
        name: "Коля",
        age: 33,
        sex: "чоловіча",
    }, 
    {
        name: "Жора",
        age: 99,
        sex: "чоловіча",
    }]
let onlyMaleUsers = users.filter(user => user.sex === "чоловіча")
console.log(onlyMaleUsers)

// 3

let myArray = ["travel", "hello", "eat", "ski", "lift",1,2,3,null,undefined,NaN,{a : "lol", b:1},[4,5,6,"seven"],function(){}]

function filterBy(array, type){
    result = []
    for(let i of array){
        if(typeof i !== type){
            result.push(i)
        }
    }
    return result
}

console.log(filterBy(myArray,"object"))